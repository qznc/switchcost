
static inline uint64_t rdtsc(void)
{
	uint64_t res;
	__asm__ __volatile__ (
			"cpuid\n" // prevent out-of-order execution of rdtsc
			"rdtsc\n"
			: "=A" (res)
			: "a" (0)
			: "ebx", "ecx", "memory");
	return res;
}

static uint64_t _glob_cycle;

static inline void update_counter(void)
{
	_glob_cycle = rdtsc();
}

static inline uint64_t measure_counter(void)
{
	uint64_t now = rdtsc();
	return now - _glob_cycle;
}

static inline uint64_t min(uint64_t x, uint64_t y)
{
	return x < y ? x : y;
}

typedef struct pt_cfg {
	unsigned threadid;
	unsigned loops;
	uint64_t avg_switch_cost;
	uint64_t min_switch_cost;
} pt_cfg;

typedef struct glob_cfg {
	unsigned thread_count;
	unsigned loops;
	pt_cfg* configs;
} glob_cfg;

static glob_cfg* configure(int argc, char *argv[])
{
	unsigned thread_count = 2;
	if (argc > 1) {
		thread_count = atoi(argv[1]);
	}
	unsigned loops = 10*1000*1000;
	if (argc > 2) {
		loops = atoi(argv[2]);
	}

	pt_cfg *cfgs = malloc(sizeof(pt_cfg)*thread_count);
	for (unsigned i=0; i<thread_count; ++i) {
		pt_cfg *cfg = &cfgs[i];
		cfg->threadid = i;
		cfg->loops = loops;
	}

	glob_cfg *config = malloc(sizeof(glob_cfg));
	config->thread_count = thread_count;
	config->loops = loops;
	config->configs = cfgs;
	return config;
}

static void finish_measure(glob_cfg *config)
{
	const unsigned thread_count = config->thread_count;
	pt_cfg *cfgs = config->configs;

	uint64_t avg_t = 0;
	uint64_t min_t = ULLONG_MAX;
	for (unsigned i=0; i<thread_count; ++i) {
		avg_t += cfgs[i].avg_switch_cost;
		min_t = min(cfgs[i].min_switch_cost, min_t);
	}
	avg_t = avg_t / thread_count;
	printf("Context switch in cycles: %"PRId64" avg,  %"PRId64" min\n", avg_t, min_t);

	free(cfgs);
	free(config);
}

#define BEFORE_YIELD\
	pt_cfg *cfg = (pt_cfg*)arg;\
	const unsigned loops = cfg->loops;\
	uint64_t diff_sum = 0;\
	uint64_t diff_min = ULLONG_MAX;\
	for (unsigned i=0; i<loops; ++i) {\
		update_counter();

#define AFTER_YIELD\
		uint64_t timed = measure_counter();\
		diff_sum += timed;\
		diff_min = min(diff_min, timed);\
	}\
	cfg->avg_switch_cost = diff_sum / loops;\
	cfg->min_switch_cost = diff_min;
