#include <sched.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <inttypes.h>

#include "libtask/task.h"
#include "measure.h"

static unsigned task_count;
static Rendez monitor;

static void task_measuring(void *arg)
{
	BEFORE_YIELD;
	taskyield();
	AFTER_YIELD;
	if (0 == --task_count)
		taskwakeup(&monitor);
}

void taskmain(int argc, char *argv[])
{
	glob_cfg *config = configure(argc,argv);
	const unsigned thread_count = config->thread_count;

	/* measure */
	task_count = thread_count;
	memset(&monitor, 0, sizeof(Rendez));
	for (unsigned i=0; i<thread_count; ++i) {
		pt_cfg *cfg = &(config->configs[i]);
		taskcreate(task_measuring, cfg, 1024);
	}
	tasksleep(&monitor);

	finish_measure(config);
}
