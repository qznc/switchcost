CFLAGS=-std=c99 -Werror -Wall -Wextra -O3

LOOPS ?= 10000000
THREADS ?= 2
PROGRAMS=m_pthread m_libtask m_pth

all:

libtask/libtask.a:
	cd libtask && make libtask.a

libpth.a:
	cd pth && ./configure && make
	cp pth/.libs/libpth.a libpth.a

m_pthread: m_pthread.c measure.h
	${CC} ${CFLAGS} -pthread $< -o $@

m_libtask: m_libtask.c libtask/libtask.a measure.h
	${CC} ${CFLAGS} $< libtask/libtask.a -o $@

m_pth: m_pth.c libpth.a measure.h
	${CC} ${CFLAGS} $< libpth.a -pthread -o $@

.PHONY: test clean distclean

clean:
	cd libtask && make clean
	cd pth && make clean
	rm -f libpth.a

distclean: clean
	rm -f ${PROGRAMS}

test: ${PROGRAMS}
	$(foreach prog,$^, taskset 1 ./${prog} ${THREADS} ${LOOPS};)
