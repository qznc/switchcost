#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <inttypes.h>

#include "pth/pth.h"
#include "measure.h"

static void* pth_measuring(void *arg)
{
	BEFORE_YIELD;
	pth_yield(NULL);
	AFTER_YIELD;
	return NULL;
}

int main(int argc, char *argv[])
{
	glob_cfg *config = configure(argc,argv);
	const unsigned thread_count = config->thread_count;

	/* measure */
	pth_init();
	pth_t *threads = malloc(sizeof(int)*thread_count);
	for (unsigned i=0; i<thread_count; ++i) {
		pt_cfg *cfg = &(config->configs[i]);
		pth_attr_t attr = pth_attr_new();
		threads[i] = pth_spawn(attr, pth_measuring, cfg);
	}
	for (unsigned i=0; i<thread_count; ++i) {
		pth_join(threads[i], NULL);
	}

	finish_measure(config);
	return 0;
}
