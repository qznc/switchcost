#include <pthread.h>
#include <sched.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <inttypes.h>

#include "measure.h"

static void* pthread_measuring(void *arg)
{
	BEFORE_YIELD;
	sched_yield();
	AFTER_YIELD;
	return NULL;
}

int main(int argc, char *argv[])
{
	glob_cfg *config = configure(argc,argv);
	const unsigned thread_count = config->thread_count;

	/* measure */
	pthread_t *threads = malloc(sizeof(pthread_t)*thread_count);
	for (unsigned i=0; i<thread_count; ++i) {
		pt_cfg *cfg = &(config->configs[i]);
		pthread_create(&threads[i], NULL, pthread_measuring, cfg);
	}
	for (unsigned i=0; i<thread_count; ++i) {
		pthread_join(threads[i], NULL);
	}

	finish_measure(config);

	return 0;
}
